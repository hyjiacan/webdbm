import storage from "../storage"

const storageKey = 'connections'

class Connection {
    id = -1

    label = ""

    host = ""

    port = 0

    user = ""

    password = ""

    database = ""

    /**
     * 连接超时时长，单位为秒
     */
    timeout = 30

    remark = ""

    encoding = "UTF-8"

    timezone = "Asia/Shanghai"

    unicode = true

    useSSL = false

    constructor() {
    }
}

/**
 *
 * @returns {{[id]: [connection]}}
 */
function get() {
    return storage.get(storageKey) || {}
}

/**
 *
 * @param {Connection} connection
 */
function set(connection) {
    const connections = get()

    if (connection.id && connections.hasOwnProperty(connection.id)) {
        connections[connection.id] = connection
    } else {
        let maxId = 0
        for (let i = 0; i < connections.length; i++) {
            const con = connections[i]
            if (con.id === connection.id) {
                Object.assign(con, connection)
                exists = true
            }
            if (con.id > maxId) {
                maxId = con.id
            }
        }

        connection.id = maxId + 1

        if (!exists) {
            connections.push(connection)
        }


    }
    storage.set(storageKey, connections)
}

function remove() {

}
