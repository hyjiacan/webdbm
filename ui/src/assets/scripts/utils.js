import store from "@/store";

export default {
  parseDataset(dataset) {
    if (!dataset) {
      return []
    }
    return dataset.map(this.parseDatatable)
  },
  parseDatatable({columns, data, rowsAffected}) {
    return {
      columns,
      rowsAffected,
      data: data.map(cells => {
        const row = {}

        columns.forEach((col, i) => {
          row[col] = cells[i]
        })
        return row
      })
    }
  },
  getQueryParams(connection, database, args) {
    if (arguments.length <= 1) {
      args = connection
      connection = store.getters.activeInfo.connection
      database = store.getters.activeInfo.database
    }
    return {
      options: {
        host: connection.host,
        port: connection.port,
        user: connection.user,
        password: connection.password,
        database: database ? database.name : 'mysql',
        timeout: connection.timeout,
        unicode: connection.unicode,
        encoding: connection.encoding,
        timezone: connection.timezone,
        useSSL: connection.useSSL,
      },
      ...(args || {})
    }
  }
}
