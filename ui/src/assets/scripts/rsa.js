import { JSEncrypt } from "jsencrypt";

let rsaKey;
let jsencrypt;

function init(publicKey) {
  rsaKey = `-----BEGIN PUBLIC KEY-----
${publicKey}
-----END PUBLIC KEY-----`;
  jsencrypt = new JSEncrypt();
  jsencrypt.setPublicKey(rsaKey);
}

function encrypt(connectionInfo) {
  const data = JSON.stringify(connectionInfo)
  return jsencrypt.encrypt(data);
}

export default {
  init,
  encrypt
};
