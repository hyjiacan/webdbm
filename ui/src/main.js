import { createApp } from 'vue'
import App from './App.vue'

// import ElementPlus from "element-plus";
// import "element-plus/dist/index.css";
// import zhCn from "element-plus/es/locale/lang/zh-cn";
import store from './store'
import { Pane, Splitpanes } from 'splitpanes'
import 'splitpanes/dist/splitpanes.css'
import './assets/styles/vars.css'
import rsa from '@/assets/scripts/rsa'
import apis from '@/assets/apis'
import { ElMessage } from 'element-plus'

const appElement = document.getElementById('app')
appElement.innerHTML = '正在加密传输通道 ...'
apis.base.securityKey().then((pk) => {
  appElement.innerHTML = '正在初始化 ...'
  rsa.init(pk)

  const app = createApp(App)
  // app.use(ElementPlus, {
  //   locale: zhCn
  // });
  app.use(store)

  app.component(Splitpanes.name, Splitpanes)
  app.component(Pane.name, Pane)
  app.mount('#app')
})
  .catch((e) => {
    ElMessage.error(`生成传输密钥失败： ${e.message}`)
    appElement.innerHTML = e.message
  })
