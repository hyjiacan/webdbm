package com.hyjiacan.h2c

class ExecuteResult {
    var rowsAffected = 0
    var success = false
    var message: String? = ""
    var columns: Array<String>? = null
    var data: Array<Array<Any?>>? = null
}