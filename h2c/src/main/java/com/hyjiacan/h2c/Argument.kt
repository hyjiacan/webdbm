package com.hyjiacan.h2c

class Argument() {
    enum class ArgumentTypes {
        Value,
        KeyValue
    }

    var type: ArgumentTypes = ArgumentTypes.Value
    var raw: String = ""
    var key: String = ""
    var value: String = ""

    companion object {
        fun parse(str: String): Argument {
            val arg = Argument()
            arg.raw = str

            if (str[0] == '-') {
                // -u=username
                arg.key = str[1].toString()
                if (str.length > 2) {
                    arg.value = str.substring(3)
                }
                arg.type = ArgumentTypes.KeyValue
            } else {
                // filepath
                arg.value = str
                arg.type = ArgumentTypes.Value
            }
            return arg
        }
    }
}
