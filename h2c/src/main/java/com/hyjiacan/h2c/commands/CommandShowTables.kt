package com.hyjiacan.h2c.commands

import com.hyjiacan.h2c.Cmd
import com.hyjiacan.h2c.ExecuteResult
import com.hyjiacan.h2c.H2

class CommandShowTables : ICommand {
    override val name: String
        get() = "tables"
    override val description: String
        get() = "列出当前数据库的所有用户表。例: tables %test%"

    override fun test(string: String): Boolean {
        return string.startsWith(name)
    }

    override fun run(string: String, db: H2): Boolean {
        Cmd.pl("正在执行 ...")
        val namePattern = if (string == name) {
            null
        } else {
            string.substring(name.length).trim().uppercase()
        }
        var sql = "select TABLE_NAME from information_schema.tables where table_schema='PUBLIC'"
        if (namePattern != null) {
            sql += " and table_name like '${namePattern}'"
        }

        val result = db.execute(sql)

        Cmd.printResult(result)
        return true
    }
}