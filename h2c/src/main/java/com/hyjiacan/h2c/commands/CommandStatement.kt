package com.hyjiacan.h2c.commands

import com.hyjiacan.h2c.Cmd
import com.hyjiacan.h2c.H2

class CommandStatement : ICommand {
    override val name: String
        get() = "statement"
    override val description: String
        get() = "运行指定的 SQL 语句。"

    override fun test(string: String): Boolean {
        return true
    }

    override fun run(string: String, db: H2): Boolean {
        Cmd.pl("正在执行 ...")
        val result = db.execute(string)
        Cmd.pl("执行完成")
        Cmd.printResult(result)
        return true
    }
}