package com.hyjiacan.h2c.commands

import com.hyjiacan.h2c.Cmd
import com.hyjiacan.h2c.H2

class CommandExit : ICommand {
    override val name: String
        get() = "exit"
    override val description: String
        get() = "退出 h2c"

    override fun test(string: String): Boolean {
        return string == name
    }

    override fun run(string: String, db: H2): Boolean {
        Cmd.pl("Bye !")
        return false
    }
}