package com.hyjiacan.h2c.commands

import com.hyjiacan.h2c.H2

interface ICommand {
    val name: String
    val description: String

    fun test(string: String): Boolean

    /**
     * @return 返回 true 表示可以继续执行, false 表示需要终止执行
     */
    fun run(string: String, db: H2): Boolean
}