package com.hyjiacan.h2c


open class App {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            println("欢迎使用 h2c，这是一个 H2 数据库的命令行工具")
            println("注意：所有命令都需要使用 ; 结束。")
            Cmd.printLine()
            if (!Cmd.parse(args)) {
                return
            }

            try {
                Workbench().startup()
            } catch (e: Exception) {
                Cmd.printLine()
                Cmd.pl("出现未知错误: ${e.message}")
            }
        }
    }
}