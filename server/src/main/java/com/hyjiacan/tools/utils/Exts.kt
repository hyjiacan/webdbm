package com.hyjiacan.tools.utils

object Exts {
    fun ByteArray.endsWith(suffix: ByteArray): Boolean {
        if (suffix.size > this.size) return false
        return this.copyOfRange(this.size - suffix.size, this.size).contentEquals(suffix)
    }
}