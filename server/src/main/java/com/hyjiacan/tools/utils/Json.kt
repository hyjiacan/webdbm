package com.hyjiacan.tools.utils

import com.alibaba.fastjson2.JSON
import com.alibaba.fastjson2.JSONObject

object Json {
    fun stringify(obj: Any): String {
        return JSON.toJSONString(obj)
    }

    fun <T> parse(str: String, clazz: Class<T>): T {
        return JSON.parseObject(str, clazz)
    }

    fun <T> parseArray(str: String, clazz: Class<T>): List<T> {
        return JSON.parseArray(str, clazz)
    }

    fun parseArray(str: String): List<*> {
        return JSON.parseArray(str)
    }

    fun parse(str: String): JSONObject {
        return JSON.parseObject(str)
    }
}