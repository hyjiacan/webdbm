package com.hyjiacan.tools.apps.apitool.handlers

import com.hyjiacan.tools.apps.apitool.handlers.models.ApiRequestForm
import com.hyjiacan.tools.http.models.RequestContext
import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response
import java.io.BufferedReader
import java.io.IOException
import java.io.OutputStream


class ApiTestHandler : HttpHandler {
    val client = OkHttpClient()
    override fun handle(exchange: HttpExchange) {
        val request = RequestContext(exchange, false)

        var form = ApiRequestForm(request.form)

        // 转发请求
        val response = forwardRequest(form)

        // 设置响应头和状态码
        exchange.responseHeaders.add("Content-Type", "application/json")
        exchange.sendResponseHeaders(response.statusCode, response.body.length.toLong())

        // 写入响应体
        val os: OutputStream = exchange.responseBody
        os.write(response.body.toByteArray())
        os.close()
    }


    private fun forwardRequest(params: ApiRequestForm): HttpResponse {
        val method = params.method
        val url = params.url
        val contentType = params.contentType
        val headers = params.headers
        val body = params.body

        val response: Response
        when    (method) {
            "GET" -> {
                response = getRequest(url)
            }
        }

        // status
        // statusText
        // body
        // headers
        val responseCode = 200
        val responseBody = "OK"

        return HttpResponse(responseCode, responseBody)
    }

    @Throws(IOException::class)
    private fun getRequest(url: String): Response {
        val request: Request = Request.Builder().url(url).build()
        return client.newCall(request).execute()
    }

//    @Throws(IOException::class)
//    private fun postRequest(url: String, json: String, contentType: String) {
//        val body: RequestBody = create(json, MediaType.get(contentType))
//        val request: Request = Builder()
//            .url(url)
//            .post(body)
//            .addHeader("Custom-Header", "HeaderValue") // 添加自定义头
//            .build()
//        client.newCall(request).execute().use { response ->
//            System.out.println("POST Response Code: " + response.code())
//        }
//    }
//
//    @Throws(IOException::class)
//    private fun putRequest(url: String, file: File, contentType: String) {
//        val requestBody: RequestBody = create(file, MediaType.get(contentType))
//        val request: Request = Builder()
//            .url(url)
//            .put(requestBody)
//            .addHeader("Custom-Header", "HeaderValue") // 添加自定义头
//            .build()
//        client.newCall(request).execute().use { response ->
//            System.out.println("PUT Response Code: " + response.code())
//        }
//    }
//
//    @Throws(IOException::class)
//    private fun deleteRequest(url: String, contentType: String) {
//        val request: Request = Builder()
//            .url(url)
//            .delete()
//            .addHeader("Content-Type", contentType) // 添加 Content-Type 头
//            .build()
//        client.newCall(request).execute().use { response ->
//            System.out.println("DELETE Response Code: " + response.code())
//        }
//    }
//
//    @Throws(IOException::class)
//    private fun headRequest(url: String) {
//        val request: Request = Builder().url(url).head().build()
//        client.newCall(request).execute().use { response ->
//            System.out.println("HEAD Response Code: " + response.code())
//        }
//    }
//
//    @Throws(IOException::class)
//    private fun optionsRequest(url: String) {
//        val request: Request = Builder().url(url).method("OPTIONS", null).build()
//        client.newCall(request).execute().use { response ->
//            System.out.println("OPTIONS Response Code: " + response.code())
//        }
//    }

    data class HttpResponse(val statusCode: Int, val body: String)
}