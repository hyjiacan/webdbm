package com.hyjiacan.tools.apps.dbm.models

class ConnectionModel {
    var id: Int = -1

    var label: String = ""

    var host: String = ""

    var port: Int = 0

    var user: String = ""

    var password: String = ""

    var database: String = ""

    /**
     * 连接超时时长，单位为秒
     */
    var timeout: Int = 30

    var remark: String = ""

    var encoding: String = "UTF-8"

    var timezone: String = "Asia/Shanghai"

    var unicode: Boolean = true

    var useSSL: Boolean = false
}