package com.hyjiacan.tools.apps.dbm.models

import com.hyjiacan.tools.utils.Json


class DbmRequestForm(body: Map<String, Any>) {
    var options: String = ""
    var table: String = ""
    var statement: String = ""
    var args: List<ParamItem>
    var fetchOffset = 0
    var fetchSize = 0

    init {
        this.fetchOffset = body.getOrDefault("fetchOffset", "0").toString().toInt()
        this.fetchSize = body.getOrDefault("fetchSize", "0").toString().toInt()
        this.options = body.getOrDefault("options", "").toString()
        this.statement = body.getOrDefault("statement", "").toString()
        this.table = body.getOrDefault("table", "").toString()
        this.args = Json.parseArray(body.getOrDefault("args", "[]").toString(), ParamItem::class.java)
    }
}