package com.hyjiacan.tools.apps.dbm.models

class DataTable {
    var tables: ArrayList<String> = ArrayList()
    var databases: ArrayList<String> = ArrayList()
    var columns: List<String> = ArrayList()
    var data: MutableList<Any> = ArrayList()
    var rowsAffected = 0
    var insertId: Long = -1
    var query: Boolean = false
}