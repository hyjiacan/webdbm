package com.hyjiacan.tools.http.exceptions

import com.hyjiacan.tools.http.exceptions.HTTPException

class NotFound : HTTPException("Not Found", 404)