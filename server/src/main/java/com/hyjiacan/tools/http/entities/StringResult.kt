package com.hyjiacan.tools.http.entities

class StringResult {
    var success = false
    var message: String? = null
    var data: String? = null
}