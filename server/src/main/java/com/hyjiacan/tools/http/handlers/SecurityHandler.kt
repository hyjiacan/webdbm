package com.hyjiacan.tools.http.handlers

import com.hyjiacan.tools.http.models.RequestContext
import com.hyjiacan.tools.http.entities.StringResult
import com.hyjiacan.tools.utils.Rsa
import java.security.NoSuchAlgorithmException

class SecurityHandler : AjaxHandler() {
    override fun get(request: RequestContext): Any {
        if (request.path == "/api/v1/security/key") {
            return getKey()
        }
        throw Exception("Path not found")
    }

    override fun post(request: RequestContext): Any {
        TODO("Not yet implemented")
    }

    override fun put(request: RequestContext): Any {
        TODO("Not yet implemented")
    }

    override fun delete(request: RequestContext): Any {
        TODO("Not yet implemented")
    }

    @Throws(NoSuchAlgorithmException::class)
    fun getKey(): StringResult {
        Rsa.makeKeys()
        val result = StringResult()
        result.success = true
        result.data = Rsa.publicKey
        return result
    }
}