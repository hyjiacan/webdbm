package com.hyjiacan.tools.http.handlers

import com.hyjiacan.tools.http.models.RequestContext
import com.hyjiacan.tools.http.entities.QueryResult


class UserHandler : AjaxHandler() {
    override fun get(request: RequestContext): Any {
        when (request.path) {
            "/api/v1/user" -> {
                return queryLogin(request)
            }

            else -> {
                throw Exception("Path not found")
            }
        }
    }

    override fun post(request: RequestContext): Any {
        when (request.path) {
            "/api/v1/user" -> {
                return doLogin(request)
            }

            else -> {
                throw Exception("Path not found")
            }
        }
    }

    override fun put(request: RequestContext): Any {
        TODO("Not yet implemented")
    }

    override fun delete(request: RequestContext): Any {
        when (request.path) {
            "/api/v1/user" -> {
                return doLogout(request)
            }

            else -> {
                throw Exception("Path not found")
            }
        }
    }

    fun queryLogin(request: RequestContext): QueryResult {
        val result = QueryResult()
        if (request.session == null) {
            return result
        }
        result.success = request.session.userId != null
        return result
    }

    fun doLogin(request: RequestContext): QueryResult {
        val result = QueryResult()

        return result
    }

    fun doLogout(request: RequestContext): QueryResult {
        val result = QueryResult()

        return result
    }
}