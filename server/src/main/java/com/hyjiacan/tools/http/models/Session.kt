package com.hyjiacan.tools.http.models

class Session(val sessionId: String) {
    var userId: String? = null
    var data: MutableMap<String, Any> = mutableMapOf()
    var lastActiveTime: Long = System.currentTimeMillis()

    fun set(key: String, value: Any) {
        data[key] = value
    }

    fun get(key: String): Any? {
        return data[key]
    }

    fun remove(key: String) {
        data.remove(key)
    }

    override fun toString(): String {
        return "<Session ${this.sessionId}:${this.userId} ...>"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        return this.hashCode() == (other as Session).hashCode()
    }

    override fun hashCode(): Int {
        return this.sessionId.hashCode()
    }
}