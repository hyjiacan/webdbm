package com.hyjiacan.tools.http.entities

class QueryResult {
    var success = false
    var message: String? = null
    var data: List<Any> = ArrayList()
}