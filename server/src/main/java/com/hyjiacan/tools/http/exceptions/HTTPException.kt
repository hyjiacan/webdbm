package com.hyjiacan.tools.http.exceptions

abstract class HTTPException(message: String?, val status: Int = 200) : Throwable(message)