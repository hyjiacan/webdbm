package com.hyjiacan.tools

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default

class Options(parser: ArgParser) {
    val showNginxConf by parser.flagging("--nginx", help = "输出 nginx 代理配置串")
    val host by parser.storing("指定 WEB 界面监听的IP地址，默认为 127.0.0.1").default("127.0.0.1")
    val port by parser.storing("指定 WEB 界面监听的端口，默认为 80") { toInt() }.default(80)
}