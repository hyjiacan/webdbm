# WebDBM

基于 Web 的免部署 MySQL 数据库管理工具

依赖: Java8

启动方法:

```shell
java -jar webdbm-*.jar --host [127.0.0.1] --port [80] --nginx
```

注：

- `[host]` 是指自定义的地址，默认为 `127.0.0.1`。
- `[port]` 是指自定义的端口，默认为 `80`。

启动后，可以通过浏览器打开 http://127.0.0.1 使用。

## 使用 nginx 代理

使用 --nginx 可以输出代理配置。

```conf
server {
  listen 443 ssl http2;

  location /webdbm/ {
    proxy_pass  http://127.0.0.1:80/;
    proxy_set_header Host $proxy_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_read_timeout 60s;
  }
}
```

## 功能计划

- 3.1.0
  - [x] 使用文件存储代替 H2 存储(加密存储)连接信息
  - [ ] 支持使用浏览器本地存储，而不是服务器存储连接信息
  - [x] 显示表的创建语句
  - [x] 优化 sql 编辑体验
- 3.2.0
  - [ ] 支持添加行数据(可视化)
  - [ ] 支持编辑行数据(可视化)
  - [ ] 支持删除行数据(可视化)
  - [ ] 通过变量查询到 `basedir`，调用 `mysqldump` 命令执行全库备份
